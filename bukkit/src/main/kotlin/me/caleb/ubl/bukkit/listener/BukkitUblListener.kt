package me.caleb.ubl.bukkit.listener

import me.caleb.ubl.Ubl
import me.caleb.ubl.bukkit.BukkitUbl
import me.caleb.ubl.bukkit.BukkitUblPlugin
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent

class BukkitUblListener(private val ubl: BukkitUbl) : Listener {

    @EventHandler
    fun onJoin(e: AsyncPlayerPreLoginEvent) {
        val entry = ubl.entries.firstOrNull { e.uniqueId == it.uuid } ?: return

        e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ubl.formatKickMessage(entry))
        BukkitUblPlugin.logger.info("The player \"${e.uniqueId}\" (${e.name}) tried to join, but they are on the UBL!")
    }

}