package me.caleb.ubl.bukkit

import me.caleb.ubl.Ubl
import me.caleb.ubl.UblEntry
import me.caleb.ubl.bukkit.util.sendConsoleMessage
import org.bukkit.BanEntry
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import java.text.NumberFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.concurrent.CompletableFuture
import java.util.function.Function
import java.util.logging.Level

class BukkitUbl(private val plugin: JavaPlugin) : Ubl(Function {
    BukkitUblPlugin.logger.log(Level.SEVERE, it.message)
    return@Function true
}) {

    val kickMessage = ChatColor.translateAlternateColorCodes('&', plugin.config.getString("ban message", "You are on the UBL!")).replace("\\n", "\n")
    val dateFormat = DateTimeFormatter.ofPattern(plugin.config.getString("date format", "MMMM d, yyyy"))

    /**
     * Get all players with the permission to be sent a notification when the UBL is updated.
     */
    internal val playersToNotifyOnUpdate: Set<Player>
        get() = Bukkit.getOnlinePlayers().filter { it.hasPermission("ubl.update.notify") }.toSet()

    override fun update(): Set<UblEntry> {
        return update(null)
    }

    fun update(additionalPlayersToNotify: Collection<Player>?): Set<UblEntry> {
        sendConsoleMessage("The UBL is starting to update.", playersToNotifyOnUpdate)

        val startTime = System.currentTimeMillis()
        val entries = super.update()

        var kicked: Set<Player>? = null
        object : BukkitRunnable() {
            override fun run() {
                kicked = kickBannedPlayers()
            }
        }.runTask(plugin)

        val endTime = System.currentTimeMillis()

        sendConsoleMessage("The UBL has been updated (${(endTime - startTime) / 1000}s). ${entries.size} entries have been retrieved. ${kicked?.size ?: 0} online players have been kicked.", if(additionalPlayersToNotify != null) playersToNotifyOnUpdate.plus(additionalPlayersToNotify) else playersToNotifyOnUpdate)

        return entries
    }

    override fun updateAsync(): CompletableFuture<Set<UblEntry>> {
        return updateAsync(null)
    }

    fun updateAsync(additionalPlayersToNotify: Collection<Player>?): CompletableFuture<Set<UblEntry>> {
        val future = CompletableFuture<Set<UblEntry>>()
        object : BukkitRunnable() {
            override fun run() {
                future.complete(update(additionalPlayersToNotify))
            }
        }.runTaskAsynchronously(plugin)
        return future
    }

    /**
     * Kicks all players that are on the UBL.
     * @return The player's that are on the UBL that have been kicked.
     */
    fun kickBannedPlayers(): Set<Player> {
        val kicked = HashSet<Player>()
        for (player in Bukkit.getOnlinePlayers()) {
            val ublEntry = entries.firstOrNull { it.uuid == player.uniqueId } ?: continue
            player.kickPlayer(formatKickMessage(ublEntry))
            BukkitUblPlugin.logger.info("${player.name} was kicked because they are on the UBL.")
            kicked.add(player)
        }
        return kicked
    }

    internal fun formatKickMessage(ublEntry: UblEntry): String {
        val completed = ublEntry.dateStarted.until(LocalDate.now())
        return kickMessage
                .replace("{{ name }}", ublEntry.name)
                .replace("{{ uuid }}", ublEntry.uuid.toString())
                .replace("{{ reason }}", ublEntry.reason)
                .replace("{{ startDate }}", dateFormat.format(ublEntry.dateStarted))
                .replace("{{ endDate }}", dateFormat.format(ublEntry.dateExpires))
                .replace("{{ caseURL }}", ublEntry.caseUrl)
                .replace("{{ totalBanDuration }}", ublEntry.banLength.days.toString())
                .replace("{{ completedBanDuration }}", completed.days.toString() + " days")
                .replace("{{ remainingBanDuration }}", LocalDate.now().until(ublEntry.dateExpires).days.toString() + " days")
                .replace("{{ percentCompletedOfSentence }}", NumberFormat.getPercentInstance().format(completed.days / ublEntry.banLength.days))
    }

}