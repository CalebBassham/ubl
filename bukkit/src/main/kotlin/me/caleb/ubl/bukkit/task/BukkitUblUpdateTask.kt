package me.caleb.ubl.bukkit.task

import me.caleb.ubl.bukkit.BukkitUbl
import org.bukkit.scheduler.BukkitRunnable

class BukkitUblUpdateTask(private val ubl: BukkitUbl) : BukkitRunnable() {

    override fun run() {
        ubl.update()
    }
}