package me.caleb.ubl.bukkit.command

import me.caleb.ubl.bukkit.BukkitUbl
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

class UblCommand(private val ubl: BukkitUbl) : CommandExecutor, TabCompleter {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(args.size == 1) {
            if(args[0].equals("update", ignoreCase = true)) {
                if(!sender.hasPermission("ubl.update")) {
                    sender.sendMessage(ChatColor.RED.toString() + "You do not have the permission to manually trigger an update to the UBL.")
                    return true
                }

                if(sender is Player) {
                    ubl.updateAsync(setOf(sender))
                } else {
                    ubl.updateAsync()
                }
                return true
            }
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<out String>): List<String>? {
        if(args.size == 1) {
            return listOf("update")
        }

        return null
    }
}