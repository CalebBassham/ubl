package me.caleb.ubl.bukkit.util

import me.caleb.ubl.bukkit.BukkitUblPlugin
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender

fun sendConsoleMessage(message: String, recipients: Collection<CommandSender>) {
    recipients.forEach { it.sendMessage(ChatColor.GRAY.toString() + org.bukkit.ChatColor.ITALIC + "[Server: $message]") }
    BukkitUblPlugin.logger.info(message)
}