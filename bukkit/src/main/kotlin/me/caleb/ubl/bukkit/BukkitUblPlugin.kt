package me.caleb.ubl.bukkit

import me.caleb.ubl.bukkit.command.UblCommand
import me.caleb.ubl.bukkit.listener.BukkitUblListener
import me.caleb.ubl.bukkit.task.BukkitUblUpdateTask
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask
import java.util.logging.ConsoleHandler
import java.util.logging.Formatter
import java.util.logging.LogRecord
import java.util.logging.Logger

class BukkitUblPlugin : JavaPlugin() {
    companion object {
        internal val logger = Logger.getLogger("UBL").apply {
            useParentHandlers = false

            val format = object : Formatter() {
                override fun format(logRecord: LogRecord): String {
                    return "[UBL] ${logRecord.message}"
                }
            }

            addHandler(ConsoleHandler().apply { formatter = format })
        }
    }

    lateinit var ubl: BukkitUbl
    private lateinit var autoUpdateTask: BukkitTask

    override fun onEnable() {
        config.options().copyDefaults(true)
        saveConfig()

        ubl = BukkitUbl(this)

        getCommand("ubl").apply {
            UblCommand(ubl).apply {
                executor = this
                tabCompleter = this
            }
        }

        Bukkit.getPluginManager().registerEvents(BukkitUblListener(ubl), this)
        autoUpdateTask = BukkitUblUpdateTask(ubl).runTaskTimerAsynchronously(this, 0, config.getLong("auto-update interval", 600) * 20)
    }

    override fun onDisable() {
        autoUpdateTask.cancel()
    }

}