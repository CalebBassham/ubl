package me.caleb.ubl

import java.util.*

class UblParseException(column: String, offeredValue: String, name: String?, uuid: UUID?, cause: Throwable? = null): RuntimeException("Could not parse column \"$column\" from offered value \"$offeredValue\" for \"$uuid\" ($name)", cause)