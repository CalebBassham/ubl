package me.caleb.ubl

import java.time.LocalDate
import java.time.Period
import java.util.*

/**
 * @author Caleb
 * @constructor
 * @param name The name of the player at the time of their ban.
 * @param uuid The UUID of the player.
 * @param reason The reason the player was banned.
 * @param dateStarted The starting date of the player's ban.
 * @param dateExpires The ending date of the player's ban.
 * @param caseUrl The url of the player's case.
 */
data class UblEntry(val name: String, val uuid: UUID, val reason: String, val dateStarted: LocalDate, val dateExpires: LocalDate, val caseUrl: String, val active: Boolean) {
    val banLength: Period = Period.between(dateStarted, dateExpires)

    val expired: Boolean
            get() = LocalDate.now().isAfter(dateExpires)
}