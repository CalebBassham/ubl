package me.caleb.ubl

import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.csv.CSVFormat
import java.io.StringReader
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import java.util.function.Function
import java.util.logging.*
import java.util.logging.Formatter
import kotlin.collections.HashSet

open class Ubl(private val exceptionHandler: Function<Throwable, Boolean> = Function { true }, private val url: String = "https://docs.google.com/spreadsheet/ccc?key=0AjACyg1Jc3_GdEhqWU5PTEVHZDVLYWphd2JfaEZXd2c&output=csv") {

    var entries = emptySet<UblEntry>()

    private val dateFormats = arrayOf(DateTimeFormatter.ofPattern("MMMM d,[ ]yyyy"))

    private val caseRegex = Regex("https?://redd\\.it/[a-z0-9]+/?")

    /**
     * Update the UBL
     */
    open fun update(): Set<UblEntry> {
        val client = OkHttpClient().newBuilder()
                .retryOnConnectionFailure(true)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(30, TimeUnit.SECONDS)
                .followRedirects(true)
                .build()

        val request = Request.Builder()
                .url(url)
                .header("Accept-Language", "en-US,en;q=0.8")
                .header("User-Agent", "Mozilla")
                .header("Referer", "google.com")
                .build()

        val response = client.newCall(request).execute()

        val data = response.body().string()

        val csv = CSVFormat.EXCEL.withHeader().parse(StringReader(data))

        val entries = HashSet<UblEntry>()

        for (row in csv) {
            val name: String
            try {
                name = row.get("IGN")
            } catch (e: Exception) {
                if (exceptionHandler.apply(UblParseException("IGN", row.get("IGN"), null, null, e))) {
                    continue
                } else {
                    return entries
                }
            }

            val uuid: UUID
            try {
                uuid = UUID.fromString(row.get("UUID"))
            } catch (e: IllegalArgumentException) {
                if(exceptionHandler.apply(UblParseException("UUID", row.get("UUID"), name, null, e))) {
                    continue
                } else {
                    return entries
                }
            }

            val reason: String
            try {
                reason = row.get("Reason")
            } catch (e: Exception) {
                if(exceptionHandler.apply(UblParseException("Reason", row.get("Reason"), name, uuid, e))) {
                    continue
                } else {
                    return entries
                }
            }

            val dateStarted: LocalDate
            try {
                dateStarted = parseDate(row.get("Date Banned").replace(160.toChar(), ' '))
            } catch (e: DateTimeParseException) {
                if(exceptionHandler.apply(UblParseException("Date Banned", row.get("Date Banned"), name, uuid, e))) {
                    continue
                } else {
                    return entries
                }
            }

            val dateExpires: LocalDate
            try {
                dateExpires = parseDate(row.get("Expiry Date").replace(160.toChar(), ' '))
            } catch (e: DateTimeParseException) {
                if(exceptionHandler.apply(UblParseException("Expiry Date", row.get("Expiry Date"), name, uuid, e))) {
                    continue
                } else {
                    return entries
                }
            }


            val case: String
            try {
                case = row.get("Case")

                if(!caseRegex.matches(case)) {
                    throw UblParseException("Case", case, name, uuid)
                }
            } catch (e: Exception) {
                if(exceptionHandler.apply(UblParseException("Case", row.get("Case"), name, uuid, e))) {
                    continue
                } else {
                    return entries
                }
            }

            entries.add(UblEntry(name, uuid, reason, dateStarted, dateExpires, case, true))
        }

        this.entries = entries

        return entries
    }

    /**
     * Asynchronously update the UBl
     */
    open fun updateAsync(): CompletableFuture<Set<UblEntry>> {
        val future = CompletableFuture<Set<UblEntry>>()
        Thread({
            future.complete(update())
        }).start()
        return future
    }

    /**
     * Determine if the UBL contains a UUID.
     * @param uuid The UUID of the player
     */
    fun contains(uuid: UUID) = entries.map { it.uuid }.contains(uuid)

    /**
     * Parse a date.
     * @param date The string representation of the date.
     * @throws DateTimeParseException
     */
    private fun parseDate(date: String): LocalDate {
        var lastException: Throwable = DateTimeParseException("Could not parse date.", date, -1)

        for (format in dateFormats) {
            try {
                return LocalDate.parse(date, format)
            } catch (e: DateTimeParseException) {
                lastException = e
                continue
            }
        }

        throw lastException
    }
}