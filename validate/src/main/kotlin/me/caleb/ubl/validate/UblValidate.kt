package me.caleb.ubl.validate

import me.caleb.ubl.Ubl
import org.apache.commons.cli.*
import java.util.function.Function
import java.util.logging.*

fun main(args: Array<String>) {

    val options = Options()

    val output = Option("o", "output", true, "Output file path")
    output.isRequired = false
    options.addOption(output)

    val parser = DefaultParser()
    val helpFormatter = HelpFormatter()
    val cmd: CommandLine

    try {
        cmd = parser.parse(options, args)
    } catch (e: ParseException) {
        println(e.message)
        helpFormatter.printHelp("UBL Validate", options)

        System.exit(1)
        return
    }

    val outputFilePath = cmd.getOptionValue("output")

    val logger = Logger.getLogger("UBL-Validate").apply {

        val formatter = object : Formatter() {
            override fun format(logRecord: LogRecord): String {
                return logRecord.level.name + ": " + logRecord.message + '\n'
            }
        }

        val ch = ConsoleHandler().apply {
            setFormatter(formatter)
        }

        if (outputFilePath != null) {
            val fh = FileHandler(outputFilePath).apply {
                setFormatter(formatter)
            }

            addHandler(fh)
        }

        addHandler(ch)
        useParentHandlers = false
    }

    Ubl(Function {
        logger.log(Level.SEVERE, it.message)
        return@Function true
    }).update()
}